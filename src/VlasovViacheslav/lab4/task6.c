/*�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#define MAXFAM 20

void cleanStdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int N = 0, i = 0, age=0, young_age = 100, old_age = 0;
	char family[MAXFAM][25];
	char *youngest = 0, *oldest = 0;

	puts("How many relative do u have (1 - 20)?");

	while (scanf("%d", &N) == 0 || N > MAXFAM || N <= 0)
		{
			puts("Please input correct number of relative \n");
			cleanStdin();
		}

	puts("Enter Name and Age 'Slava 26'");

	for (i = 0; i < N; i++)
	{
		while (scanf("%s %d", family[i], &age) <2 || age<0 || age>100)
			fflush(stdin);
		if (age>old_age)
			{
				old_age = age;
				oldest = family[i];
			}
			if (age < young_age)
			{
				young_age = age;
				youngest = family[i];
			}
	}
	printf("The youngest is %s \nThe oldest is %s \n", youngest, oldest);
	return 0;
}