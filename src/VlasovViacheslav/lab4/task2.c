/*�������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������*/

#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#define SIZE 256

int main()
{
	char str[SIZE] = { 0 };
	char *pstr[SIZE] = { 0 };
	int i = 0, j = 0;
	int inWord = 0;

	puts("Enter a line:");
	fgets(str, SIZE, stdin);

	for (i = 0; i <= (int)strlen(str)-1; i++)					  // ������ �� ����� �������
	{
		while (str[i] == ' ') i++;									// ���������� �������
		{
			if (str[i] != '\n')										 // �������� �� ����� �����
			{
				pstr[inWord] = &str[i];								 // ����� ������ ����� � ������ ����� �����
				while (str[i] != ' ' && str[i] != '\n') i++; 
				inWord++;											 // ��� ������ ����� ����������� ������� ���
			}
			else
				break;
		}
	}

	for (i = inWord-1; i >= 0; i--)								 // ��� ������ ����� ������ ������ ����� �� ���������� ����
	{
		j = 0;
		while (*(pstr[i] + j) != ' ' && *(pstr[i] + j) != '\n')  //����� ����� �� ������� �� ������� �������� �� ������� ����������
		{
			putchar(*(pstr[i] + j));
			j++;
		}
		putchar(' ');
	}
	return 0;
}