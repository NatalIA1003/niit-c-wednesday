/*�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.*/


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define M 256
#define N 100

int comp(const void *a, const void *b)
{
	return strlen(*(char**)a) - strlen(*(char**)b);
}

int main()
{
	FILE *out, *in;
	char str[N][M] = { 0 };
	char *pstr[N] = { 0 };
	int count = 0, i = 0;

	out = fopen("Output.txt", "wt");
	in = fopen("Input.txt", "rt");

	if (in == 0 || out == 0) 
	{
		perror("File");   
		return 1;
	}


	while (fgets(str[count], N, in) != 0 && i < N)
	{
		pstr[count] = str[count];
		count++;
	}

	qsort(pstr, count, sizeof(char*), comp);

	for (i = 0; i<count; i++)
		fprintf(out, "%s", pstr[i]);

	fclose(in);
	fclose(out);
	return 0;
}