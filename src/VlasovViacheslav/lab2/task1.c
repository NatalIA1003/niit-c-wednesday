// �������� ������� �����

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void cleanStdin()
{
	char ch;
	do ch = getchar();
	while (ch != '\n' && ch != EOF);
}

int main()
{
	int H = 0, t = 0;
	float h = .0f, g = 9.81f, distance;
	clock_t now, start;
	
	while (1)
	{
		puts("Input H (If you don't want wait forever please <1000)");

		if (scanf("%d", &H) == 1)
			break;
		else
		{
			puts("inputs wrong");
			cleanStdin();
		}

	}

	start = clock();
	while (1)
	{

		now = clock();
		t = (now - start) / 1000;
		distance = (H - g*t*t / 2);
		if (distance > 0)
		{
			printf("t = %2.d c, H = %.1f m\n", t, distance);
			while (clock() < now + 1000);
		}
		else
		{
			puts("BABAH");
			break;
		}
	}
	return 0;
}