// ������ �����

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void cleanStdin()
{
	char ch;
	do ch = getchar();
	while (ch != '\n' && ch != EOF);
}

int main()
{
	int value,c;
	clock_t now;
	srand(time(0));

	value = rand() % 100;
	//printf("%d\n", value); 
	now = clock(); 
	
	puts("Guess the number 1 to 100");
	while (1)
	{
		if (scanf("%d", &c) == 1)
		{
			if (c == value)
			{
				puts("you are right!!!");
				break;
			}
			else
				puts(c > value ? "less" : "more"); // ��������� �������� �� ������ ������ ������ ��� ������
		}
		else
		{
			puts("enter the number");
			cleanStdin();
		}
	}
	return 0;
}