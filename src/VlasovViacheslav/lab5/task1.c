/*�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 256

int getWords(char *str, char **words, int inWord)
{
	for (int i = 0; i <= (int)strlen(str) - 1; i++)					  // ������ �� ����� �������
	{
		while (str[i] == ' ') i++;									// ���������� �������
		{
			if (str[i] != '\n')										 // �������� �� ����� �����
			{
				words[inWord] = &str[i];								 // ����� ������ ����� � ������ ����� �����
				while (str[i] != ' ' && str[i] != '\n') i++;
				inWord++;											 // ��� ������ ����� ����������� ������� ���
			}
			else
				break;
		}
	}
	return inWord;
}

void random_Words(char **words, int inWords)
{
	clock_t now;
	srand((unsigned int)time(0));

	for (int i = inWords - 1; i > 0; --i)
	{
		int new = rand() % (i + 1);
		char *tmp = words[i];
		words[i] = words[new];
		words[new] = tmp;
	}
}

void printWord(char **words, int inWord)
{
	int i = inWord - 1;
	for (; i >= 0; i--)												 // ��� ������ ����� ������ ������ ����� �� ���������� ����
	{
		int j = 0;
		while (*(words[i] + j) != ' ' && *(words[i] + j) != '\n')  //����� ����� �� ������� �� ������� �������� �� ������� ����������
		{
			putchar(*(words[i] + j));
			j++;
		}
		putchar(' ');
	}
}

int main()
{
	int i = 0, j = 0;
	int inWord = 0;
	char str[256] = { NULL };
	char *words[20] = { NULL };

	puts("Input string;");
	fgets(str, SIZE, stdin);

	inWord = getWords(str, words, inWord);  // ��������� ������� ������ ������ �����
	random_Words(words, inWord);			// ���������� ������ ������� ������� ������ ����
	printWord(words, inWord);				// ����� �� �����

	return 0;
}

