/*�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 100

int getWords(char *str, char **words, int inWord)		// ���������� ������� ������ ������� ����� ������. ���������� �� � ������ ����������
{
	for (int i = 0; i <= (int)strlen(str) - 1; i++)					  
	{
		while (str[i] == ' ') i++;									
		{
			if (str[i] != '\n')										
			{
				words[inWord] = &str[i];								
				while (str[i] != ' ' && str[i] != '\n') i++;
				inWord++;											
			}
			else 
				break;
		}
	}
	return inWord;										// ���������� ���������� ���� � ����������� ��� ����������� �������
}

void random_Words(char **words, int inWord)				// ���������� ����� ������� ���� � �����������.
{
	srand((unsigned int)time(NULL));
	for (int i = inWord - 1; i > 0; --i)
	{
		int new = rand() % (i + 1);
		char *tmp = words[i];
		words[i] = words[new];
		words[new] = tmp;
	}
}

void printWord(char **words, int inWord)				// �������� ������ �����������.
{
	for (int i = inWord - 1; i >= 0; i--)								 
	{
		int j = 0;
		while (*(words[i] + j) != ' ' && *(words[i] + j) != '\n') 
		{
			putchar(*(words[i] + j));
			j++;
		}
		putchar(' ');
	}
}

int main()
{
	char str[N] = { 0 };
	char *words[N] = { 0 };
	int count = 0, inWord = 0;
	FILE *in = fopen("Input.txt", "rt");

	if (in == 0)
	{
		perror("File");
		return 1;
	}

	while (fgets(str, N, in) != 0 && count < N)
	{
		inWord = getWords(str, words, inWord);
		random_Words(words, inWord);
		printWord(words, inWord);

		putchar('\n');
		inWord = 0;
	}
	fclose(in);
	return 0;
}

