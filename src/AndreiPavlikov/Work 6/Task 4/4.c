#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <conio.h>


#define  M 10

int *a=NULL;
int summa(int k )
{	
	if (k >= 0)
	{
		if (k == 0) return (a[0]);
		else return(summa(k - 1) + a[k]);	 
	}
	return(0);
}


int main()
{
	int i, N,sum1, sum2;
	clock_t t, t1, t2;
	N = (int)pow((double)2, (double)M);
	a = (int*)malloc(N*sizeof(int));
	
	for (i = 0; i < N; i++) a[i] = rand();

	t1 = clock();

	sum1 = 0;

	for (i = 0; i < N; i++) sum1 = sum1 + a[i];
	
	t2 = clock(); t = (float)(t2 - t1) / CLOCKS_PER_SEC;
	
	printf("\nTradition: sum=%d  time=%d", sum1, t);

	t1 = clock();

	sum2=summa(N-1);

	t2 = clock(); t = (float)(t2 - t1) / CLOCKS_PER_SEC;

	printf("\nRekurs: sum=%d  time=%d\n", sum2, t);
	
	free(a);

	return 0;
}
