#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int fib(int n)
{
	if ((n == 0) || (n == 1)) return 1;
	else
		return (fib(n - 1) + fib(n - 2));
}


int main()
{
	int F, fb;
	scanf("%d", &F);
	fb = fib(F);
	printf("%d\n", fb);
	
	return 0;
}