#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 
unsigned long int k;

void coll(unsigned long int n)
{
	k++;
	if (n!=1)
	{
		if (n % 2 == 0) coll(n / 2);
		else coll(3 * n + 1);
	}
}


int main()
{
	unsigned long int i, kmax = 0, nmax = 0;
	for ( i = 2; i <1000000; i++)
	{
		k = 0; 
		coll(i);
		if (k > kmax)
		{
			kmax = k;
			nmax = i;
		}
	}
	printf("MAX COLL=%d for N=%d\n", kmax, nmax);
	return 0;
}
