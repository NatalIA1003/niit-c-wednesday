#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <windows.h>
#define M 14
#define N 30


char lab[M][N] = { 0 };


void print(int m, int n)
{
	system("cls");
	lab[m][n] = 'X';
	for (int i = 0; i < M; i++)
	{
		for (int j = 0; j < N; j++)
		{
		  printf("%c", lab[i][j]);
		}

	}printf("\n");
	Sleep( 200);
}

int xod(int m, int n)
{

	if (lab[m][n] == '#' || lab[m][n] == '-') return 0;
	if (m == 0 || m == M-2 || n == 0 || n == N-2)
	{
		print(m, n);
		return 1;
	}
	
	print(m, n);
	lab[m][n] = '-';
	if (xod(m, n + 1))
	{
		lab[m][n] = '-';
		return 1;
	}
	if (xod(m, n - 1))
	{
		lab[m][n] = '-';
		return 1;
	}
	if (xod(m - 1, n))
	{
		lab[m][n] = '-';
		return 1;
	}
	if (xod(m + 1, n))
	{
		lab[m][n] = '-';
		return 1;
	}
	return 0;
}

int main()
{
	FILE *file1;
	file1 = fopen("c:\\Temp\\FIB.txt", "r");
	if (file1==NULL)
	{
		printf("ERROR FILE");
		return 1;
	}
	for ( int i = 0; i < M;  i++)
	{
		for ( int j = 0; j < N; j++)
		{
			lab[i][j] = fgetc(file1);
		}
	}

	xod((M-1)/2, (N-1) / 2);
	

	fclose(file1);
	return 0;
}