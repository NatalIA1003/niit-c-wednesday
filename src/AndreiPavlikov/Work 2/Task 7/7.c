#define _CRT_SECURE_NO_WARNINGS
#define R 100
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	int  i, j, ind, m = 0, n = 0, ksim[40];                             // ����������� ����������
	char str[R] = { 0 }, sim[40] = { 0 };                                // ���������� ������� � ��� ���������
	printf("Input string: ");
	while ((str[n] = getchar()) != '\n')				// ������ ��������� ������ � ������
	{
		n++;
	}
	for (i = 1; i <= 39; i++) ksim[i] = 0;                 //���������� 0 � ������
	for (i = 0; i < n; i++)                            // ���� �� �������� ������ 
	{
		ind = 0; j = 1;
		while ((j <= m) && (ind == 0))                 // �������� ����� ������������� ��������
		{
			if (sim[j] == str[i])
			{
				ksim[j]++; ind = 1;					// ���������� ����� �������� �� +1 
			}
			else j++;                            // ������� � ���������� �������
		}
		if (ind == 0)							// ���������� ������� � ������         
		{
			m++; sim[m] = str[i]; ksim[m] = 1; j = m;
		}

	}
	printf("In string: %s", str);												// ������ �������� ������
	for (j = 1; j <= m; j++)  printf("\n Symbol %c meets %d\n", sim[j], ksim[j]); //������ ���������� ������� �������� ����������� � ������
	return 0;
}