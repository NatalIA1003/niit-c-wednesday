// звездочки пирамида
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int i, p, z, N, j, k;
	const int str = 39;
	printf("Vvedite visoty piramidy- ");
	scanf("%d", &N);
	p = str;
	z = 1;
	for (i = 1; i <= N; i++)
	{
		for (j = 1; j <= p; j++) printf(" ");
		for (k = 1; k <= z; k++) printf("*"); printf("\n");
		p--;
		z = z + 2;
	}
	return 0;
}