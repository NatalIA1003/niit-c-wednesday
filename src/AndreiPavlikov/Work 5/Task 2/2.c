#define _CRT_SECURE_NO_WARNINGS
#define MG 60
#define NV 60
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <Windows.h>
#include<conio.h>

void Matrix(char(*pmatrix)[MG], int m, int n)
{
	int j = 0, i = 0;
	for (i = 0; i < m / 2; i++)
		for (j = 0; j < n / 2; j++)
		{
			if ((rand() % 2) == 0) pmatrix[i][j] = '*';
			{
				pmatrix[i][n - j] = pmatrix[i][j];
				pmatrix[m - i][j] = pmatrix[i][j];
				pmatrix[m - i][n - j] = pmatrix[i][j];
			}
		}
}

int main()
{
	int end=1;
	char arr[MG][NV] = { ' ' };
	while (end>0)
	{
		for (int i = 0; i < MG; i++)
			for (int j = 0; j < NV; j++) arr[i][j] = ' ';
		Matrix(arr, MG, NV);
		system("cls");
		for (int i = 0; i < MG; i++)
		{
			printf("\n");
			for (int j = 0; j < NV; j++)
				printf("%c", arr[i][j]);
		}
		Sleep(1000);
	}
	return 0;
}