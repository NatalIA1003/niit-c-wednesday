#include <stdio.h>
# define N 20
int main()
{
    int i = 0, j = N - 1, s;
    char temp, arr[N] = "65dfgh8g67dse45gym78";
    while (i < j){
        for (s = i; s < j; s++) {
            if (arr[i] >= 'a' && arr[i] <= 'z') i++;
            else break;
        }

        for (s = j; s > i; s--) {
            if (arr[j] >= '0' && arr[j] <= '9')	j--;
            else break;
        }

        if (i > j) break;
        temp = arr[i]; arr[i] = arr[j]; arr[j] = temp;
                
    }
    for (i = 0; i < N; i++)
        printf("%c", arr[i]);

    return 0;
}
