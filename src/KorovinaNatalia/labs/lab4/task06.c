/*Lab 04 Task 06. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������.��������� ������
���������� ������ �������� � ������ ������� ������������.
��������� : ����� ������� ������ ����� ��� �������� ��� � ��� ��������� : young � old,������� �� ���� �����, ��������� � ������� ��������.*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define N 80
#define M 100


void clean()
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
    printf("Input error!\n");
}

int main()
{
    char str[N][M] = { 0 };
    char *old, *young;
    int count = 0, currentAge=0;
    int i = 0, k = 0, maxAge=0, minAge=150;

    while (1)
    {
        puts("Enter the number of family members:");
        if (scanf("%d", &count) == 1)
            while (i < count)
            {
                printf("Enter the name and age of %d family member, using format 'Name age':\n", (i + 1));
                if (scanf("%s %d", &str[i], &currentAge) == 2)
                {
                    if (currentAge > maxAge)
                    {
                        maxAge = currentAge;
                        old = str[i];
                    }
                    if (currentAge < minAge)
                    {
                        minAge = currentAge;
                        young = str[i];
                    }
                    i++;
                }

                else
                {
                    clean();
                    continue;
                }                
             }

        else
        {
            clean();
            continue;
        }

        break;
        }

    printf("The oldest one is: %s. The younger one is: %s\n", old, young);
    return 0;
}