/*Lab03. Task04. �������� ���������, ������� ������� ����� ����� �� �������� ������ */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define MAXRANK 4

int main()
{
    char str[80];
    int i = 0;
    int iCurrentSum = 0, iWholeSum = 0, iPosition = 0;

    puts("Enter a line:");
    fgets(str, 80, stdin);
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1] = 0;

    while (str[i]) //���� ������� ������ �� ����� ����
    {
        if (str[i] >= '0' && str[i] <= '9')
        {
            iCurrentSum = iCurrentSum * 10 + str[i] - '0';							//������� ����� (�� ���� ������ ������)

            if (str[i + 1] < '0' || str[i + 1] > '9' || iPosition == MAXRANK)		// ���� ������ ���� �� ����� ��� ��������� ������������ ����� ��������
            {
                iWholeSum = iWholeSum + iCurrentSum;							// ������� ����� ����������� � ����� ����� 
                iCurrentSum = 0;												// � ���������� 
            }

            iPosition++;
        }
        i++;
    }
    printf("%d", iWholeSum);
    return 0;
}