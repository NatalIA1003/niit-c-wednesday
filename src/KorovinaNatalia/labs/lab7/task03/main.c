/*Lab 07. Task 3. �������� ���������, ������� ������ ������� ������������� ��������
��� ������������� �����, ��� �������� ������� � ��������� ������.
��������� ������ �������� �� ����� ������� �������������, ��������������� �� �������� �������*/

#define _CRT_SECURE_NO_WARNINGS
#include "symbols.h"
#include <stdio.h>
#include <stdlib.h>
#include <locale.h> 
#define N 300
int comp(const void *a, const void *b)
{
    return ((((TSYMBOL*)b)->frequency) - (((TSYMBOL*)a)->frequency));
}

int main(int argc, char**argv)
{
    setlocale(LC_ALL, "Rus");
    FILE *fpIn;
    int i = 0, ch, unicS=0, count=0;
    TSYMBOL arr[N] = {0};
    PSYMBOL parr[N] = { 0 };

    if (argc < 2)
    {
        puts("Define filename");
        exit(1);
    }

    fpIn = fopen(argv[1], "rt");
    if (fpIn == NULL)
    {
        perror("File:");
        exit(2);
    }

    while ((ch = fgetc(fpIn)) != EOF)
    {
        count += 1;
        arr[ch].symbol = (char)ch;
        arr[ch].frequency += 1;
    }

    qsort(arr, N, sizeof(TSYMBOL), comp);
    i = 0;
    while (arr[i].frequency != 0)
    {
        parr[i] = &arr[i];
        arr[i].frequency = arr[i].frequency / count;
        PrintSymbol(&arr[i]);
        i++;
        unicS += 1;
    }

    fclose(fpIn);
    return 0;
}


