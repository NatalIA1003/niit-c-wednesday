/*Task 1. Body Analysis Calculator*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

int main()
{
	char cOutput[] = "You are";
	char cAnswers[][20] = { "unerweight", "normal","overweight" };
	int ch, iChoice;
	float fWeight, fHeight, fIMT;
	char cSex = 0;
	
	while (1)
	{
		printf("Enter your sex (m/f), height in centimeters, and weight in kilograms, using format \"sex-height-weight\" please:\n"); // �������� ������������ ��������� ������
		if (scanf("%c-%f-%f", &cSex, &fHeight, &fWeight) == 3)

		{
			fHeight = fHeight / 100.0f;		// ������� ����� � �����
			fIMT = fWeight / (pow(fHeight, 2.0f)); // ���������� ������� ����� ����
			if (cSex == 'm' || cSex == 'M')				// ��� ������

			{
				if (fIMT < 20)
					iChoice = 0;
				else if (fIMT >= 20 && fIMT <= 25)
					iChoice = 1;
				else
					iChoice = 2;

				printf("%s %s man.\n", cOutput, cAnswers[iChoice]);
			}
			else if (cSex == 'f' || cSex == 'F')	// ��� ������
				printf("You are beautiful woman!\n");
			else									// ���� ������� ������������ ����� - ������, ������ �����.
			{
				puts("Incorrect sex!");
				do
					ch = getchar();
				while (ch != '\n'&& ch != EOF);
				continue;
			}
			break;
		}
																// ������ - ������� ������������ ������
		else
		{
			puts("Input error!");
			do
				ch = getchar();
			while (ch != '\n'&& ch != EOF); 
		}

	}
	return 0;
}
