/*Lab 06. Task 01. �������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� �������*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#define M 80
#define K 80
int arr[M][K] = { 0 };

int draw(int x, int y, int N) // �-�� ��������� ������ ���������
{
   
    if (N == 0)
        arr[x][y] = '+';
    else
    {   
        draw(x + (int)pow(3, N - 1), y, N - 1);
        draw(x - (int)pow(3, N - 1), y, N - 1);
        draw(x, y - (int)pow(3, N - 1), N - 1);
        draw(x, y + (int)pow(3, N - 1), N - 1);
        draw(x, y, N - 1);
    }
    return 0;
}

void arrprint()         // �-�� ������ ������� 
{
    int i = 0, j = 0;
    for (i = 0;i < M;i++)
    {
        for (j = 0;j < K;j++)
            printf("%c", arr[i][j]);

        printf("\n");
    }
}

void clean()  // �-�� �������
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
    printf("Input error! ");
}

int main()
{
    
    int N = 0;
    int x, y;
    
    while (1)
    {
        printf("Input N:\n");
        if ((scanf("%d", &N)) == 0)
            clean();
        else
            break;
    }

    x = M / 2;  //���������� �������� '����'
    y = K / 2;
    draw(x, y, N);
    arrprint();
    return 0;

}