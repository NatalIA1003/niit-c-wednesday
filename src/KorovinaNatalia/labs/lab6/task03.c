/*Lab 06. Task 03. ������� ����� � ������ */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define M 80
char arr[M] = { 0 };

void clean()  // �-�� �������
{
    int ch;
    do
        ch = getchar();
    while (ch != '\n'&& ch != EOF);
    printf("Input error! ");
}

int convert(int n, int i) // ����������� ������ �������������� ����� � ������ (���������� �������)
{
    int p;

    if (n != 0)
    {
        p = (n % 10);
        arr[i] = p + '0';
        convert((n / 10), (i + 1));
    }
    return 0;
}

void reverse(char *arr, int sign) // �������� ����������� ��� ����������� �������
{
    int i=0, j = strlen(arr) - 1;
    char tmp;
    if (sign == 1) //���� ����� ���� ������������� - �������� ���������� �� ���� ����� ����� ��������
        i = 1;
    for (i; i < j; i++, j--)
    {
        tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}

int main()
{
    int n = 0;
    int i = 0;
    int sign = 0;  // ���������� ��������������� ���� ���������� �����

    while (1)
    {
        printf("Input your number:\n");
        if ((scanf("%d", &n)) == 0)
            clean();
        else
            break;
    }

    if (n < 0) // ���� ���� ������� ������������� �����, ������ ���������� � ������
    {
        arr[i] = '-';
        n = -n;
        sign = 1;  // ����, ������������ ������� ����� � �����
        i++;
    }
    convert(n, i); //������������ � ������ ��������� �������� ��������
    reverse(arr,sign); // �������� ����������� ��� ����������� � ���������� ����� �������
    printf("%s", arr); 
    return 0;
}