/*Lab 06. Task 07. �������� */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>

#define N 256  //����� ������ � ���������
char **arr;
int flag = 0; //���� ������ ������
int nor = 0; //����� ����� � ���������

int getLineNumber(FILE *fp) //������� ���������� ����� � ����� � ����������
{
    int count = 0;
    char tmp[N];
    while (fgets(tmp, N, fp))
        count++;
    return count;
}

void printMaze() //����� �������� �������� �����������
{
    clock_t now;
    system("cls");

    for (int i = 0;i < nor;i++)
    {
        printf("%s", arr[i]);
        if (arr[i][(strlen(arr[i])) - 1] != '\n')
            printf("\n");
    }
    now = clock();
    while (clock() < now + 100);
}

void maze(int x, int y)  //����������� �-�� ����������� ��������� � ������ ���������� �� ������
{
    arr[x][y] = '.';
    printMaze(); //������ �������� ��������� �� ������� ������
    if (x <= 0 || y <= 0 || y >= (strlen(arr[1]) - 2) || x >= nor - 1) //������� ������ �� ������� ���������
    {
        flag = 1;
    }

    else
    {
        if (flag == 0 && arr[x + 1][y] == ' ')
            maze(x + 1, y);
        if (flag == 0 && arr[x][y - 1] == ' ')
            maze(x, (y - 1));
        if (flag == 0 && arr[x][y + 1] == ' ')
            maze(x, (y + 1));
        if (flag == 0 && arr[x - 1][y] == ' ')
            maze(x - 1, y);
    }
}

int main()
{
    FILE *fp;
    int i = 0, j = 0; 
    fp = fopen("maze.txt", "rt");
    if (fp == 0)
    {
        perror("File");
        return 1;
    }

    nor = getLineNumber(fp); //������� ����� ����� � ���������
    
    arr = (char**)malloc(nor*sizeof(char*)); //��������� ������ ��� ������
    for (i = 0;i < nor;i++)
        arr[i] = (char*)malloc(N*sizeof(char));
    rewind(fp);

    i = 0;
    while ((i < nor && fgets(arr[i], N, fp) != 0)) //������ ��������� �� ����� � ������
        i++;
    arr[nor / 2][strlen(arr[1]) / 2] = '.';     //���������� ����� ������ ��������
    maze(nor / 2, strlen(arr[1]) / 2); // ����� �-�� ���������� ���������. ���������� �� ������ ���������.
    printMaze(); //������ ��������� ��������� ���������
    for (i = 0;i < nor;i++)
        free(arr[i]);
    free(arr);
    fclose(fp);
    return 0;
}





