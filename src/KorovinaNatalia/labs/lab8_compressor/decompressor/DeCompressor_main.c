#define _CRT_SECURE_NO_WARNINGS
#include "DeCompressor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 300

int main(int argc, char**argv)
{
    unsigned char buf[8] = { 0 };
    FILE *fpIn, *fp101, *fpOutDeComp;
    int i = 0, j = 0, ch, unicS = 0, count10 = 0, tail = 0, tailDeComp = 0;
    unsigned long long count = 0;
    TSYMBOL syms[N] = { 0 };
    PSYMBOL psyms[N] = { 0 };
    PSYMBOL root = { 0 };
    char formatIn[10] = { 0 };
    char nameIn[N] = { 0 };
    char newName[N] = { 0 };
    char formatSign[4]; //������� �������
    long size; // ������ �����

    if (argc < 2)
    {
        puts("Define filename");
        exit(1);
    }

    fpIn = fopen(argv[1], "rb");
    if (fpIn == NULL)
    {
        perror("File:");
        exit(1);
    }

    while (*(argv[1] + i) != '.') //����������� ����� ��������� �����
    {
        nameIn[i] = *(argv[1] + i);
        i++;
    }

    fp101 = fopen("101_D.txt", "w+t"); // �������� ���������� ����� 
    if (fp101 == NULL)
    {
        perror("File:");
        exit(2);
    }

    fread(&formatSign, sizeof(formatSign), 1, fpIn); // 1.������ ������� �������
    fread(&unicS, sizeof(unicS), 1, fpIn);//2.������ ���-�� ���������� ��������
    for (i = 0;i < unicS;i++) //3. ������ ������� ���� + ������� �������������
    {
        fread(&syms[i].symbol, sizeof(char), 1, fpIn);
        fread(&syms[i].frequency, sizeof(float), 1, fpIn);
        psyms[i] = &syms[i];

    }
    fread(&tail, sizeof(tail), 1, fpIn); // 4.������ ����� ������
    fread(&size, sizeof(size), 1, fpIn); // 5.������ ������� ��������� �����
    fread(&formatIn, sizeof(formatIn), 1, fpIn); // 6. ������ ���������� ��������� �����
    root = buildTree(psyms, unicS); //���������� ������
    while ((ch = fgetc(fpIn)) != EOF) //����������, �������� 101 �����
    {
        Unpack((char)ch, buf);
        fwrite(&buf, sizeof(buf), 1, fp101);
    }

    fclose(fpIn);
    rewind(fp101);
    sprintf(newName, "%s.%s", nameIn, formatIn); //�������� ����� ����������.�����
    fpOutDeComp = fopen(newName, "wb");
    if (fpOutDeComp == NULL)
    {
        perror("File:");
        exit(2);
    }

    //����������� ������ � ��������� � ��������
    for (long i = 0; i < size;i++)
        SearchTree(root, fp101, fpOutDeComp);
    while ((ch= fgetc(fp101)) != EOF)
        tailDeComp += 1;
    if (tailDeComp != (8 - tail))
    {
        printf("Wrong end of file!");
        exit(3);
    }

    //����������� ������� ���������������� �����
    rewind(fpOutDeComp);
    fseek(fpOutDeComp, 0, SEEK_END);
    long sizeDeComp = ftell(fpIn);
    if (sizeDeComp != size)
    {
        printf("Decompressing error!");
        exit(4);
    }

    rewind(fpIn);

    fclose(fp101);
    fclose(fpOutDeComp);
    if (remove("101_D.txt") == -1)
    {
        puts("Error of deleting");
        exit(5);
    }
    return 0;
}


