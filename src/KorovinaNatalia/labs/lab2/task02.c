/*Task2. Guess the number!*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
	int iValue, iVariant;
	char ch;
	int count = 0;
	srand(time(0));


	printf("Guess the number from 1 to 100!\n");
	while (1)
	{
		
		iValue = rand() % 100 + 1; //numbers from 1 to 100
		while (1)
		{
			printf("Input your answer:\n");
			if (scanf("%d", &iVariant) == 1)

			{
				if (iVariant < iValue)
				{
					printf("My number is greater than yours. Try again!\n");
					continue;
				}
				else if (iVariant > iValue)
				{
					printf("My number is less than yours. Try again!\n");
					continue;
				}
				else
				{
					printf("You are absolutely right!\n");
					break;
				}
				
			}
			else
			{
				puts("Input error!");
				do
					ch = getchar();
				while (ch != '\n'&& ch != EOF);

			}
		}
		break;
	}
	return 0;


}
