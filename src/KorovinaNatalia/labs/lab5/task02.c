/*Lab05. Task 02. �������� ��������� �����������*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 40

void clearArr(char (*arr)[N])   // ���������� ������� ���������
{
    int i,j;
    for (i = 0;i < N;i++)
        for (j = 0;j < N;j++)
            arr[i][j] = ' ';
}

void fill1Arr(char(*arr)[N]) // �������� ������� ������� ���������
{
    srand(time(0));
    int symType, i, j;
    
    for (i = 0;i < (N/2);i++)
        for (j = 0;j < (N/2);j++)
        {
            if ((symType = rand() % 8 + 1) == 1)
                arr[i][j] = '*';
        }
}

void fill2Arr(char(*arr)[N]) // �������� ������� 2 ���������
{
    int i, j;

    for (i = 0 ;i < N/2;i++)
        for (j = 0;j < N/2;j++)
        {
           arr[i][N/2+j] = arr[i][N/2-j-1];
        }
}

void fill3Arr(char(*arr)[N]) // �������� ������� 3 ���������
{
    int i, j;
    for (i = 0;i < N / 2;i++)
        for (j = 0;j < N / 2;j++)
     
        {
            arr[N/2+i][j] = arr[N/2-i-1][j];

        }
}

void fill4Arr(char(*arr)[N]) // �������� ������� 4 ���������
{
    int i, j;
    for (i = 0;i < N / 2;i++)
        for (j = 0;j < N / 2;j++)

        {
            arr[N / 2 + i][N/2+j] = arr[N / 2 - i-1][N/2-j-1];
        }
}

void printArr(char(*arr)[N])
{
    int i = 0, j = 0;
    for (i = 0; i < N;i++)
    {
        for (j = 0;j < N;j++)
            printf("%c", arr[i][j]);
        printf("\n");
    }
}

int main()
{
    char cSymbol[N][N] = { 0 };
    clock_t now;
    while (1)
    {
        clearArr(cSymbol); // ������� �������
        fill1Arr(cSymbol); //���������� 1 ���������
        fill2Arr(cSymbol); //���������� 2 ���������
        fill3Arr(cSymbol); //���������� 3 ���������
        fill4Arr(cSymbol); //���������� 4 ���������
        system("cls"); // ������� ������
        printArr(cSymbol); // ������ �������
        now = clock();
        while (clock() < now + 2000); // ��������� ��������

    }

    
}