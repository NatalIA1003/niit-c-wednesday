//
//  main.c
//  Lab-4. Task-6
//
//  Created by Natalya Ivanova on 20.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу, которая запрашивает количество родственников в семье, а потом позволяет ввести имя родственника и его возраст. Программа должна определить самого молодого и самого старого родственника
 Замечание: Нужно завести массив строк для хранения имён и два указателя: young и old, которые по мере ввода, связывать с нужными строками.*/
#include <stdio.h>
#include <string.h>
#define N 256
#define M 256
#define LIMIT 50

int main()
{
    char str[N][M];
    char *old, *young;
    int count=0;
    int age=0;
    int maxAge=0;
    int minAge=100;
    int i=0;
    while(1)
    {

        puts("Enter the number of family members:");
        if (scanf("%d", &count) == 1 && count>0 && count<=LIMIT)
        {
            while (i < count)
            {
                printf("Enter the name and age of %d family member, using format 'Name age':\n", (i + 1));
                    if (scanf("%s %d", &str[i], &age) == 2)
                        {
                            if (age > maxAge)
                            {   maxAge = age;
                                old = str[i];
                            }
                            if (age < minAge)
                            {
                                minAge = age;
                                young = str[i];
                            }
                            i++;
                        }
                    else
                    {
                        printf("Input error!\n");
                        
                    }
                printf("The oldest one is: %s-%d.\nThe younger one is: %s-%d\n", old, maxAge, young, minAge);
            }
        }
        else
        {
            printf("Input error!\n");
            
        }
        break;
    }
        return 0;
}