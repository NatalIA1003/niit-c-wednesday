//
//  main.c
//  Lab-4. Task-4
//
//  Created by Natalya Ivanova on 20.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Изменить программы для поиска самых длинных последовательностей в массиве с использованием указателей вместо числовых переменных.
 Замечание: Аналогично можно изменить тексты всех программ с использованием массивов так, чтобы доступ к данным осуществлялся через указатели.*/
#include <stdio.h>
#include <string.h>
#define N 80
int main()
{
    char str[N];
    char *pB, *pE, *newB, *newE;
    int i=0;
    puts("Hello! Enter your string");
    fgets(str, N, stdin);
    pB=pE=newB=newE=str;
    
    while(str[i])
    {
        newB=newE=&str[i];
        while (str[i]==str[i+1])
        {
            newE=newE+1;
            i++;
        }
        if((newE-newB)>(pE-pB))
        {
            pB=newB;
            pE=newE;
        }
        i++;
    }
    printf("Max string is:");
    for(i=0; i<=(pE-pB);i++)
        printf("%c",*pB);
    putchar('\n');
    printf("The max leght in string is: %d\n",(pE - pB+1));
    return 0;
}

