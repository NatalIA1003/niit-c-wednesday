//
//  main.c
//  Lab-4. Task-3
//
//  Created by Natalya Ivanova on 19.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу, которая запрашивает строку и определяет, не является ли строка палиндромом (одинаково читается и слева направо и справа налево)
 Замечание:
 Цель задачи - применить указатели для быстрого сканирования строки с двух концов*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 80

int main()
{
    char str[N];
    char *pstr[N] = {0};
    int i=0;
    int j=0;
    int flag=0;
    puts("Enter your string!");
    fgets(str, N, stdin);
    while(str[i])
    {
        pstr[i]=&str[i];
        i++;
    }
    i=0;
    j = strlen(str) - 1;
    while(pstr[i]<pstr[j])
    {
        if(*pstr[i] == *pstr[j])
        {
            pstr[i]++;
            pstr[j]--;
            flag=1;
        }
        else
        {
            flag=0;
            printf("This string is not polindrom!\n");
            break;
        }
    }
    if(flag==1)
    {
        printf("This string is polindrom!\n");
    }
    return 0;
}

