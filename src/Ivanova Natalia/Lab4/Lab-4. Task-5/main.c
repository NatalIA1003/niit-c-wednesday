//
//  main.c
//  Lab-4. Task-5
//
//  Created by Natalya Ivanova on 20.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу, сортирующую строки (см. задачу 1), но использующую строки, прочитанные из текстового файла. Результат работы программы также записывается в файл.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 256
#define M 256

int compare(const void*str1, const void*str2)
{
    return strlen(*(char**)str2)-strlen(*(char**)str1);
}

int main()
{
    char str[N][M];
    char *pstr[N] = { 0 };
    int i = 0;
    int j=0;
    FILE *fp, *out;
    puts("Hello!");
    
    fp=fopen("write.txt","rt");
    out=fopen("write2.txt","wt");
    if(fp==0 || out==0)
    {
        perror("File cann't be open!");
        return 1;
    }
    
    while(fgets(str[i], M, fp))
    {
        pstr[i]=str[i];
        i++;
    }
    qsort(pstr,i,sizeof(char*), compare);
    for(j=0; j<i; j++)
    {
        fprintf(out, "%s", pstr[j]);
    }
    fclose(fp);
    fclose(out);
    return 0;
}

