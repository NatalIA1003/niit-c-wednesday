//
//  main.c
//  Lab-4. Task-2
//
//  Created by Natalya Ivanova on 19.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу, которая с помощью массива указателей выводит слова строки в обратном порядке
 Замечание:
 Вместе со строкой создается массив указателей на char, в который заносятся
 адреса первых символов каждого слова (альтернатива - адреса первого и по- следнего символов). Затем мы организуем вывод новой строки, используя этот массив из указателей*/

#include <stdio.h>
#include <string.h>
#define N 80
#define M 50

void prWord(char*pstr)
{
    while (*pstr!=' ' && *pstr!='\n' && *pstr){
        putchar(*pstr++);}
}

int main()
{
    char str[N];
    char *pstr[M];
    int inWord=0;
    int count=0;
    int i=0;
    puts ("Enter your line");
    fgets(str,N,stdin);
    while (str[i])
    {
        if(str[i]!=' ' && inWord==0)
        {
            inWord=1;
            pstr[count++]=str+i;
        }
        else if(str[i]==' ' && inWord==1)
        {
            inWord=0;
        }
        i++;
    }
    for(i=count-1; i>=0; i--)
    {
        prWord(pstr[i]);
        putchar(' ');
    }
    putchar('\n');
    return 0;
}
