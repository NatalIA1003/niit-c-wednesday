//
//  main.c
//  Lab-4. Task-1
//
//  Created by Natalya Ivanova on 18.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написатьпрограмму,котораяпозволяетпользователюввестинесколь- ко строк с клавиатуры, а затем выводящую их в порядке возраста- ния длины строки.
 Замечание:
 Строки вводятся до появления пустой строки и записываются в двумерный
 символьный массив. Одновременно происходит создание и заполнение массива указателей на char. После окончания ввода программа сортирует укзатели в массиве и выводит строки в соответствии с отсортированными указателями.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 80
#define M 50

int compare(const void*str1, const void*str2)
{
    return strlen(*(char**)str2)-strlen(*(char**)str1);
}

int main()
{
    char str[N][M];
    char *pstr[N] = { 0 };
    int i = 0;
    int j=0;
    puts("Hello!");
    while(i<N && *fgets(str[i], M, stdin) !='\n')
    {
        pstr[i]=str[i];
        i++;
    }
    qsort(pstr,i,sizeof(char*), compare);
    for(j=0; j<i; j++)
    {
        printf(pstr[j]);
    }
    return 0;
}

