#include <stdio.h>
#include <string.h>

int main()
{
    char str[] = "  A big  blak   bug  ";
    int i = strlen(str) - 1;
    int j = 0;
    while (str[i]==' ')
    {
         str[i] = '\0';
         i--;
    }
    while (i >= 0)
    {
        if (str[i] == ' ' && str[i - 1] == ' ')
        {
            j = i;
            while (str[j])
            {
                str[j] = str[j + 1];
                j++;
            }
        }
        i--;
    }
    printf("%s", str);
    return 0;
}      