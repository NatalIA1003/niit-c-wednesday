#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define g 9.81

int main()

{
    int t;
    float h;
    clock_t now;

    puts("Enter, please height:");
    scanf("%f", &h);
    for (t = 0; (h - (g*t*t) / 2) >= 0; t++)
    {
        printf("t = %3d\a  h = %6.2f\n", t, h - (g*t*t) / 2);
        now = clock(); 
        while (clock() < now + 1000);
    }
    printf("Hello!\n");
    return 0;
}

