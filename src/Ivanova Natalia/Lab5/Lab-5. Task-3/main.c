//
//  main.c
//  Lab-5. Task-3
//
//  Created by Natalya Ivanova on 23.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу, переставляющую случайным образом симво- лы каждого слова каждой строки текстового файла, кроме первого и последнего, то есть начало и конец слова меняться не должны.
 Замечание:
 Программа открывает существующий тектстовый файл и читает его построч- но. Для каждой строки выполняется разбивка на слова и независимая обра- ботка каждого слова.*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 256

void wordMIX(char *pstr)
{
    
    int m=0, i, r;
    char s;
    while (pstr[m]!=' ') m++;
    if (m > 2)
    {
        m--;
        
        for (i = 1; i < m - 1; i++)
        {
            r = rand() % (m - i);
            s = pstr[r];
            pstr[r] = pstr[m - i];
            pstr[m - i] = s;
        }
        
    }
}

int main()
{
    
    int n = 0, kW, i=0;
    char *pstr = NULL;
    char str[N] = { 0 };
    FILE *fp, *out;
    fp = fopen("/.write.txt", "rt");
    out = fopen("out.txt", "wt");
    if(fp==NULL)
    {
        perror("FILE not found");
        return 1;
    }
    
    printf("Enter you string: ");
    while ((str[n] = getchar()) != '\n')
    {
        n++;
    }
 
    wordMIX(pstr);
    printf("In string: %s", pstr);
    fclose(fp);
    fclose(out);
    return 0;
}

