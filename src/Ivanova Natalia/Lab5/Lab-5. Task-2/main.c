//
//  main.c
//  Lab-5. Task-2
//
//  Created by Natalya Ivanova on 23.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
//
/*Написать программу ”Калейдоскоп”, выводящую на экран изобра- жение, составленное из симметрично расположенных звездочек ’*’. Изображение формируется в двумерном символьном массиве, в од- ной его части и симметрично копируется в остальные его части.
 Замечание:
 Решение задачи протекает в виде следующей последовательности шагов:
 1) Очистка массива (заполнение пробелами)
 2) Формирование случайным образом верхнего левого квадранта (занесение ’*’)
 3) Копирование символов в другие квадранты массива
 4) Очистка экрана
 5) Вывод массива на экран (построчно)
 6) Временная задержка
 7) Переход к шагу 1*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 40

void clearArr(char(*arr)[N])
{
    int i,j;
    for(i=0; i<N; i++)
        for(j=0; j<N; j++)
            arr[i][j]=' ';
}
void createSquare1(char(*arr)[N])
{
    srand (time (0));
    int s, i, j;
    for(i=0; i<N/2; i++)
        for(j=0; j<N/2; j++)
        {
            s=rand()%N/2;
            arr[i][s]='*';
        }
}
void createSquare2(char(*arr)[N])
{
    int i, j;
    int s=N-1;
    for(i=0; i<N/2; i++)
    {
        for(j=0; j<N/2; j++)
        {
            arr[i][s--]=arr[i][j];
        }
        s=N-1;
    }
}

void createSquareOther(char(*arr)[N])
{
    int i, j;
    int s=N-1;
    for(i=0; i<N/2; i++)
    {
        for(j=0; j<N; j++)
        {
            arr[s][j]=arr[N/2-i][j];
        }
        s--;
    }
}

void printArr(char(*arr)[N])
{
    int i=0;
    int j=0;
    for(i=0; i<N; i++)
    {
        for(j=0; j<N; j++)
        {
            putchar(arr[i][j]);
            putchar('\n');
        }
    }

}

int main()
{
    char arr[N][N]={0};
    clock_t now;
    while(1)
    {
        clearArr(arr);
        createSquare1(arr);
        createSquare2(arr);
        createSquareOther(arr);
        createSquareOther(arr);
       // system("cls");
        printArr(arr);
        now=clock();
        while(clock() < now + 10000);
        
    }
}
