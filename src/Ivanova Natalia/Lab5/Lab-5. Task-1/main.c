//
//  main.c
//  Lab-5. Task-1
//
//  Created by Natalya Ivanova on 23.04.16.
//  Copyright © 2016 Natalya Ivanova. All rights reserved.
///**Написать программу, которая принимает от пользователя строку и выводит ее на экран, перемешав слова в случайном порядке.*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 100

int getWord(char* str, char** pstr)
{
    int i=0;
    int count=0;
    int inWord=0;
    while(str[i])
    {
        if(str[i]!=' ' && inWord==0)
        {
            inWord=1;
            pstr[count++]=&str[i];
        }
        else if(str[i]==' ' && inWord==1)
        {
            inWord=0;
        }
        i++;
    }
    return count;
}
void mixWord(char** pstr, int count)
{
    srand(time(0));
    int i, k;
    char* tmp;
    int inWord=0;
    
    for(i=0; i<count; i++)
    {
        k=rand()%(count-i);
        tmp=pstr[i];
        pstr[i]=pstr[k];
        pstr[k]=tmp;
    }
}

void printWord(char* pstr)
 {
    for(;*pstr && *pstr!=' ' && *pstr!='\n';pstr++)
        putchar(*pstr);
    putchar(' ');
 }

int main(int argc, const char * argv[])
{
    int i=0;
    int count=0;
    char str[N];
    char* pstr[N];
    
    printf("Hello! Please, inter enter some words, whith i can mix!\n");
    fgets(str, N, stdin);
    if(str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
    count=getWord(str, pstr);
    mixWord(pstr, count);
        while(pstr[i])
        {
            printWord(pstr[i]);
            i++;
        }
        putchar('\n');
        return 0;
}
