#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#define M 80

int main()
{
	int numbers[M];
	short int i=0,sum=0,minnum, plusnum;
	srand(time(NULL)|clock());
	/*printf("Enter string: ");
	fgets(string,M,stdin);*/
	while(numbers[i]!='\0')
	{
		numbers[i]=rand()%256-128;
		i++;
	}
	for(i=0;i<M;i++)
	{
		if(numbers[i]<0)
		{
			minnum=i;
			break;
		}
	}
	for(i=M;i>0;i--)
	{
		if(numbers[i]>0)
		{
			plusnum=i;
			break;
		}
	}
	for(i=minnum;i<=plusnum;i++)
	{
		sum+=numbers[i];
	}
	printf("\nSum of the numbers in a string = %d", sum);
	getch();
	return 0;
}