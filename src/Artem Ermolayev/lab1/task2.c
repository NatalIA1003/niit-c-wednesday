#include <stdio.h>

int main()
{
    int i, j, k;
    printf("Enter the time in the format HH:MM:SS\n");
    scanf("%d:%d:%d", &i, &j, &k);
    if((i>24 || i<0) || (j>59 || j<0) || (k>59 || k<0))
    {
        printf("Syintax error");
   // else
       // printf("%d:%d:%d", i, j, k);
       return 0;
    }
    if((i>0 && i<3 && j<59) || (i>21 && i<24))
    {
        printf("Good night!\n");
    }
    if(i>4 && i<11 && j<59)
    {
        printf("Good morning!\n");
    }
    if(i>11 && i<15 && j<59)
    {
        printf("Good noon!\n");
    }
    if(i>16 && i<20 && j<59)
    {
        printf("Good evening!\n");
    }
    return 0;
}
