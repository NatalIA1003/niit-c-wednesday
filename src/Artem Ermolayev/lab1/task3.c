//Ermolaev A/lab1/task3/
//Transfer values degree and radian/
//
//
#include <stdio.h>
#include <conio.h>
/*
float deg2rad(float &in_value)           //Declaring functions
{
	return (in_value / 180 * 3.1415f);
}

float rad2deg(float &in_value)
{
	return (in_value * 180 / 3.1415f);
}
*/
int main()
{
	float in_value, out_value;
	char f;
	printf("Enter the data: ");
	scanf("%f%c",&in_value, &f);
	if(f=='D')
	{
		out_value = (in_value / 180 * 3.1415f);
	}
	else if (f=='R')
	{
		out_value = (in_value * 180 / 3.1415f);
	}
	else
	{
		printf("An invalid input format!");
		getch();
		return 1;
	}
	printf("%f",out_value);
	getch();
	return 0;
}