//Ermolayev Artem/lab1/task4/
//Transfer length units/
//
//
#include <stdio.h>

int main()
{
	int h_f, h_d;
	float h_s;
	printf("Enter your height in USA scale-system(with a common): ");
	scanf("%d,%d", &h_f, &h_d);
	h_s = ((h_f*12+h_d)*2.54);
	printf("%0.2f", h_s);
	getch();
	return 0;
}