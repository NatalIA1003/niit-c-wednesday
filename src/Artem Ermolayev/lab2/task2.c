#include <stdio.h>
#include <windows.h>
#include <stdlib.h>

int main()
{
	int N, X;
	srand(time());
	X=rand()%(100);
	printf("I guessed the number. Try to guess!\n");
	//printf("%d", X);
	while (1)
	{
		scanf("%d", &N);
		if (N>X)
			printf("Less!\n");
		else if (N<X)
			printf("More!\n");
		else 
		{	
			printf("Good guess!\n");
			break;
		}
	}
	return 0;
}