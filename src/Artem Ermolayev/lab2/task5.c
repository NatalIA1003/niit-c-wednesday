#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define M 8

int main()
{
	int l=0,j,t; 
	char string[M], a;	
	printf("Generate string: ");
	srand(time(NULL)|clock());
	while(l<10)
	{
		for(j=0;j<M;j++)
		{
			t=rand()%3+0;
			if(t==0)
					string[j]=('a' + rand() % ('z' - 'a'));
			else if(t==1)
					string[j]=('0' + rand() % ('9' - '0'));
			else if(t==2)
					string[j]=('A' + rand() % ('Z' - 'A'));
		}
		string[M]='\0';
		printf("\n\t\t");
		fputs(string,stdout);
		l++;
	}
	getch();
	return 0;
}