#include <stdio.h>
#include <windows.h>

int main()
{
	int t;
	float H, L=0, g=9.81f;
	printf("Enter H. H= ");
	scanf("%f", &H);
	for (t=0;;t++)
	{
		L=g*t*t/2;
		if (L>=H)
			break;
		printf("t=%02d c	h=%0.1f m\n", t, (H-L));
		Sleep(1000);
	}
	printf("BANG!");
	getch();
	return 0;
}