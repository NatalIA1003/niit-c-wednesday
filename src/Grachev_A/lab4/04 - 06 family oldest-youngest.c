// how many family members and who is the youngest/oldest from him
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#define LIMIT 100// family members limit
#define MAXL 50// max string lenght
#define YEARS 200// years old limit

int main()
{
	int i=0, number, years, years_max=0, years_min=YEARS;
	char fam[LIMIT][MAXL]={0}, *old, *young;
	printf("Hello, username! Enter how many members in your family: ");
	if((scanf("%d", &number)==1) && (number>0 && number<=LIMIT))
	{
		while(i<number)
		{
			printf("Enter name of %d member ya family & how old he is (name years): ", i+1);
			if(scanf("%s %d", &fam[i], &years)==2 && years<YEARS)
			{
				if((i==1 && years_max>years) || (i>1 && years_max>years && years_min>years))
				{
					years_min=years;
					young=&fam[i];
				}
				else if(years>years_max)
				{
					if(i==1)
					{
						years_min=years_max;
						young=&fam[0];
					}
					years_max=years;
					old=&fam[i];
				}
				i++;
			}
			else
			{
				printf("Incorrect input data for %d member ya family!\n\n", i+1);
				return -2;
			}
		}
		puts("Wait a second, please...\n");
		printf("The oldest - %s (%d years old)\nThe youngest - %s (%d years old)\n", old, years_max, young, years_min);
		return 0;
	}
	else
	{
		puts("Incorrect number for family members!\n");
		return -1;
	}
}