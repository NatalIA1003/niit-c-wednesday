// Rearrangement words in your string
#include <stdio.h>
#define N 80// max lenght

int main()
{
	int i=0, j=0, inWord=0, count=0;
	char str[N], revstr[N], *pArr[N];// str[N]-original string; revstr[N]-new string
	printf("Hello, %%username%%! Enter some words, please: ");
	fgets(str,N,stdin);
	puts("Rearrangement...");
	while(str[i])
	{
		if(inWord==0 && (str[i]!='0' || str[i]!=' '))
		{
			inWord=1;
			pArr[j]=&str[i];
			count++;
			j++;
		}
		else if(inWord==1 && (str[i]==' ' || str[i]=='\0'))
		{
			inWord=0;
		}
		i++;
	}
	puts("\nDone! New string:");
	for(j=count-1; j>=0; j--)
	{
		for(i=0; i<N; i++)
		{
			revstr[i]=*pArr[j];
			if(revstr[i]==' ' || revstr[i]=='\n')
			{
				break;
			}
			putchar(revstr[i]);
			pArr[j]++;
		}
		putchar(' ');
	}
	putchar('\n');
	return 0;
}