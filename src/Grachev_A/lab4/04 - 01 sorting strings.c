// print your strings in increase his lenght order
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXL 400// max string lenght
#define MAXS 10// max strings quantity

int main()
{
	int i=0, j, N=0;// N-strings quantity
	char str[MAXS][MAXL]={0}, *pStr[MAXS], *buf;// pStr[MAXS]-pointers to strings; buf-var for change
	puts("Hello, %username%! Enter some strings (null string for end of input): ");
	while(fgets(str[N],MAXL,stdin) && N<(MAXS-1))
	{
		if(strlen(str[N])-1==0)
		{
			break;
		}
		pStr[N]=&str[N];
		N++;
	}
	if(N!=0)
	{
		if(N>1)
		{
			puts("The end of input, start sorting strings by increase...");
			for(i=0; i<N-1; i++)// <--- compare block --->
			{
				for(j=0; j<N-1; j++)
				{
					if(strlen(pStr[j+1])<strlen(pStr[j]))
					{
						buf=pStr[j];
						pStr[j]=pStr[j+1];
						pStr[j+1]=buf;
					}
				}
			}
			puts("Done!\n");
		}
		for(i=0; i<N; i++)
		{
			printf("%s",pStr[i]);
		}
	}
	else
	{
		puts("No strings here!");
	}
	return 0;
}