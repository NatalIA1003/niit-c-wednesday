/*Unwanted word's eraser*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>

int main()
{
	int i=0, count=0, inWord=0, N;
	char str[80];
	puts("Hello, %username%! Enter the any string: ");
	fgets(str,80,stdin);
	puts("%username%, enter the number (word with this number will be erased): ");
	scanf("%d", &N);
	puts("Cleaning... wait a second, please...");
	while(str[i])
	{
		if(inWord==0 && str[i]!=' ' && str[i]!='\n')
		{
			inWord=1;
			++count;
			if(count!=N)
			{
				printf("%c", str[i]);
			}
		}
		else if(inWord==1 && str[i]!=' ' && str[i]!='\n')
		{    
			if(count!=N)
			{
				printf("%c", str[i]);
			}
		}
		else if(inWord==1 && (str[i]==' ' || str[i]=='\n'))
		{
			inWord=0;
			if(count!=N)
			{
				printf("%c", str[i]);
			}
		}
		i++;
	}
	if(N<1 || N>count)
	{
		puts("\nAttention! You enter incorrect number, string not modified!\n");
	}
	return 0;
}