// Sort by decrease symbols in your array
#include <stdio.h>
#include <string.h>

int main()
{
	int qua[128]={0}, i=0, j=0, buf;// qua-array for count symbols;  buf-variable for change
	char str[80], str2[128]={0};// str2-array for sorted symbols
	puts("Hello, %username%! Let's calculate & sort ASCII symbols in your string.\nEnter the string: ");
	fgets(str,80,stdin);
	puts("Calculation....\n");
	for(i=0; i<=(strlen(str)-1); i++)
	{
		for(j=0; j<128; j++)
		{
			if(str[i]==j)
			{
				++qua[j];
				++j;
			}
		}
	}
	puts(" Symbols calculated!\nSymbol - Founded\n");
	for(j=0; j<128; j++)
	{
		str2[j]=j;
		if(qua[j]>0)
		{
			if(str2[j]=='\n')
				printf("  \\n   -   %d\n", qua[j]);
			else if(str2[j]==' ')
				printf(" space -   %d\n", qua[j]);
			else
				printf("   %c   -   %d\n", str2[j], qua[j]);
		}
	}
	for(i=0; i<127; ++i)
	{
		for(j=0; j<127; j++)
		{
			if(qua[j+1]>qua[j])
			{
				buf=qua[j+1];
				qua[j+1]=qua[j];
				qua[j]=buf;
				buf=str2[j+1];
				str2[j+1]=str2[j];
				str2[j]=buf;
			}
		}
	}
	puts("\n\nSorting by decrease....");
	puts("\n Sorted!\nSymbol - Founded\n");
	for(j=0; j<128; ++j)
	{
		if(qua[j]>0)
		{
			if(str2[j]=='\n')
				printf("  \\n   -   %d\n", qua[j]);
			else if(str2[j]==' ')
				printf(" space -   %d\n", qua[j]);
			else
				printf("   %c   -   %d\n", str2[j], qua[j]);
		}
	}
	return 0;
}