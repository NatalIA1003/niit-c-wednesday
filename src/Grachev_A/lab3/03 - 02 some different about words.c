/* Words quantity, every word in own line, every word's lenght */
#include <stdio.h>
#include <string.h>

int main()
{
	int i=0, count=0, lcount=0, dcount=0, inWord=0;
	char str[80];
	puts("Hello, %username%! Enter the any string: ");
	fgets(str,80,stdin);
	while(str[i])
	{
		if(inWord==0 && str[i]!=' ' && str[i]!='\n')
		{
			count++;
			lcount++;
			inWord=1;
			printf("%c", str[i]);
			if(str[i]>='0' && str[i]<='9')
				dcount++;
		}
		else if(inWord==1 && str[i]!=' ' && str[i]!='\n')
		{    
			lcount++;
			printf("%c", str[i]);
			if(str[i]>='0' && str[i]<='9')
				dcount++;
		}
		else if(inWord==1 && (str[i]==' ' || str[i]=='\n'))
		{
			printf("; lenght: %d letters\n", lcount);
			inWord=0;
			lcount=0;
		}
		i++;
	}
	printf("\nWords: %d/ Digits: %d\n", count, dcount);
	return 0;
}