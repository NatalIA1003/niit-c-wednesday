/* Xst word printing */
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>

int main()
{
	int i=0, count=0, inWord=0, N;
	char str[80];
	puts("Hello, %username%! Enter the any string: ");
	fgets(str,80,stdin);
	puts("%username%, enter the number (word with this number be printed on display): ");
	scanf("%d", &N);
	puts("Wait a second, please...");
	while(str[i])
	{
		if(inWord==0 && str[i]!=' ' && str[i]!='\n')
		{
			++count;
			inWord=1;
			if(count==N)
			{
				printf("%c", str[i]);
			}
		}
		else if(inWord==1 && str[i]!=' ' && str[i]!='\n')
		{    
			if(count==N)
			{
				printf("%c", str[i]);
			}
		}
		else if(inWord==1 && (str[i]==' ' || str[i]=='\n'))
		{
			inWord=0;
		}
		i++;
	}
	if(N<1 || N>count)
	{
		puts("You enter incorrect number");
	}
	return 0;
}