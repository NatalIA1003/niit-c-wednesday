/* Recognize longest the same symbols sequence & her long */
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>

int main()
{
	int i=0, count=1, inSeq=0, count_max=0;
	char frequent, str[80]={0};
	puts("Hello, %username%! Enter the any symbols any sequence: ");
	fgets(str,80,stdin);
	puts("Wait a second, please, I'm thinkin'...");
	while(str[i])
	{
		if(i>0 && inSeq==0 && str[i]!='\n')
		{
			count=1;
			if(str[i]==str[i-1])
			{
				++count;
				inSeq=1;
				if(count>count_max)
				{
					frequent=str[i];
					count_max=count;
				}
			}
		}
		else if(inSeq==1 && str[i]!='\n')
		{    
			if(str[i]==str[i-1])
			{
				++count;
				if(count>count_max)
				{
					frequent=str[i];
					count_max=count;
				}
			}
			else
				inSeq=0;
		}
		i++;
	}
	if(count_max>1)
	{
		puts("Most longest sequence the same symbols: ");
		for(count=1; count<=count_max; count++)
		{
			printf("%c", frequent);
		}
		printf(", symbol quantity - %d\n", count_max);
	}
	else
	{
		puts("No repeat the same symbols here!");
	}
	return 0;
}