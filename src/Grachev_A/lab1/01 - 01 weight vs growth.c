// sex-growth-weight
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>

int main()
{
	char sex;
	float growth, weight, z;
	int MIN, MAX, MIN1=107, MAX1=113, MIN2=111, MAX2=117, choice1, choice2;
	char person[][10]={"Man, ", "Woman, "};
	char advice[][50]={"everything all right! Stay on the way!\n", "you astenic! You must go eat smthng!\n", "you hyperstenic! Stop eating!\n"};
	puts("Welcome, %username%!\nEnter your sex, growth and weight in following format: sex-growth-weight\nFor sex write m (man) or w (woman).\nGrowth write in sm, weight in kg.");
	if((scanf("%c-%f-%f", &sex, &growth, &weight)==3) && (sex=='m' || sex=='w') && (growth>0 && growth<300) && (weight>0 && weight<600))
	{
		printf("You entered: %c-%.2f-%.2f\n", sex, growth, weight);
		if(sex=='m')
		{
			MIN=MIN1;
			MAX=MAX1;
			choice1=0;
		}
		else
		{
			MIN=MIN2;
			MAX=MAX2;
			choice1=1;
		}
		z=growth-weight;
		if(z>=MIN && z<=MAX)
			{choice2=0;}
		else if(z>MAX)
			{choice2=1;}
		else
			{choice2=2;}
		printf("%s%s", person[choice1], advice[choice2]);
	}
	else
		{puts("Input error!\n You entered wrong data");}
	return 0;
}