// Say "hi" for daytime
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#define TIME1 05
#define TIME2 11
#define TIME3 17
#define TIME4 21

int main()
{
	int hour, minute, second, var;
	while(1)
	{
		puts("%username%, enter your current time in following format: HH:MM:SS\n");
		if((scanf("%d:%d:%d", &hour, &minute, &second)==3) && (hour>=00 && hour<=23) && (minute>=00 && minute<=59) && (second>=00 && second<=59))
		{	
			printf((hour>=TIME1 && hour<TIME2)?"Good morning, username!\n":(hour>=TIME2 && hour<TIME3)?"Good day, username!\n":(hour>=TIME3 && hour<TIME4)?"Good evening, username!\n":"Good night, username!\n");
			return 0;
		}
		else
			puts("Input error! Enter right current time");
	do
		var=getchar();
	while(var!='\n' && var!=EOF);
	}
	return 0;
}