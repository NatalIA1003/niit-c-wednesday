/*Grouping of array elements: letters-numbers*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>
#include <time.h>

int main()
{
	int i=0, j;
	char buf, str[80]={0};
	clock_t now;
	srand(time(0));
	puts("Hello, %username%! Enter random sequence english letters and numbers: ");
	fgets(str,80,stdin);
	while(str[i])// checking: string must contain only english letters & digits
	{
		if((str[i]>='0' && str[i]<='9') || (str[i]>='A' && str[i]<='Z') || (str[i]>='a' && str[i]<='z'))
		{
			;
		}
		else if(str[i]=='\n')
		{
			break;
		}
		else
		{
			puts("Enter other sequence, with only english letters and numbers\n");
			return 1;
		}
		i++;
	}
	puts("Groupping... wait a second, please...\n");
	now=clock(); // time delay for calculation
	while(clock()<now+2000);
	j=strlen(str)-1;
	for(i=0; i<j; ++i)
	{
		if(str[i]>='0' && str[i]<='9')// checking start of the string for number
		{
			for (j; j>i; --j)
			{
				if((str[j]>='A' && str[j]<='Z') || (str[j]>='a' && str[j]<='z'))// checking end of the string for letter
				{
					buf=str[i];
					str[i]=str[j];
					str[j]=buf;
					break;
				}
			}
		}
	}
	printf("%s\n", str);
	return 0;
}