/*construction of the pyramid*/
#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	int row, ch, ch1, ROWS;
	puts("Hello, %username%! Enter the number of rows of pyramid: ");
	if(scanf("%d", &ROWS)!=EOF && (ROWS>0))
	{
		row=0;
		while(row<=ROWS)
		{
			ch=0;
			while(ch<(ROWS-row))
			{
				putchar(' ');
				ch++;
			}
			ch1=0;
			while(ch1<=(2*row-2))
			{
				putchar('*');
				ch1++;
			}
			putchar('\n');
			row++;
		}
	}
	else
		puts("Input error!\n");
	return 0;
}