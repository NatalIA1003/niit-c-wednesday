#include <stdio.h>
#include <string.h>
#define SIZE 300
#define YES 1              
#define NO 0

int main()
{
    char str[SIZE];                             // user str
    unsigned char arr[128][2] = { 0 };          //for save [symbol][number]
    int i = 0, j = 0, tmp = 0;                  // for loop
    int status = NO, k = 0;                     //flag

    puts("Enter your string:");
    fgets(&str, SIZE, stdin);
    zero(str);

    while (str[i])
    {
        for (j = 0; j<strlen(str); j++)             //search symbol
        {
            if (arr[j][0] == str[i])                // if symbol have found is str
            {
                arr[j][1]++;           
                status = YES;              
                break;
            }
        }
        if (status == YES)
            status = NO;
        else                                       // if new sybmbol   
        {
            arr[k][1]++;
            arr[k][0] = str[i];
            k++;
        }
        i++;
    }
    for (i = 0; i<k-1; i++)                     //swap
    {
        for (j = i+1; j<k; j++)
            if (arr[j][1]>arr[i][1])            // change if 2 > 1       
            {
                tmp = arr[i][1];
                arr[i][1] = arr[j][1];
                arr[j][1] = tmp;
                tmp = arr[i][0];
                arr[i][0] = arr[j][0];
                arr[j][0] = tmp;
            }
    }
    // also print and skip ' '
    for (i = 0; i < k; i++){
        if (arr[i][0] == ' ');
        else
            printf("%c\t%d \n", arr[i][0], arr[i][1]);
    }
    return 0;
}