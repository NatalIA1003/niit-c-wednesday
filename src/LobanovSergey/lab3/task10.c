#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define SIZE 80
#define IN 1
#define OUT 0

int main()
{
    char str[SIZE];
    int i = 0;					   //for loop
    int status = OUT;			   // flag
    int count = 0;                 // count word
    int start, end, num=0;	       // for word 

    puts("Enter some words: ");
    fgets(str, SIZE, stdin);
    zero(str);                     // swap '\n' and '\0' on the end str

    while (1)
    {
        puts("Enter a number: ");
        if (scanf("%d", &num) == 1)
            break;
        else
            puts("Input Error! Enter a number again: ");
        buffer();
    }
    while (str[i])
    {
        if (str[i] != ' ' && status == OUT)
        {
            count++;
            status = IN;
            if (count == num)
                start = i;
            if (str[i + 1] == 0)    // if one letter in word on the end str
                end = i;
        }
        else if ((str[i] == ' ' || str[i+1] == 0) && status == IN){
            status = OUT;
            if (count == num){
                end = i;
                break;
            }
        }
        i++;
    }
    if (num > count){
        puts("Input Error! Your number out of range.\n");
        printf("The number of words = %d\n", count);
        return 0;
    }
    // copy 'from end to strlen(str)' to start
    for (i = end; i >= start; i--)
        for (int j = i; j < strlen(str); j++)
            str[j] = str[j + 1];
    
    puts("Your word below:");
    
    for (i = 0; i < strlen(str); i++)
        printf("%c", str[i]);
    putchar('\n');
    return 0;
}





