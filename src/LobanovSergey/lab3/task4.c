#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define IN 1
#define OUT 0
#define BIT 2				// the category of number
#define SIZE 80

int main()
{
	char str[SIZE];
	char ndigit[SIZE];
	int i, j=0, k=0,  first;
	int ch, sum = 0;							
	int status = OUT;				// flag inWord or outWord
	puts("Enter you string with figure: ");
	fgets(str, SIZE, stdin);
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;
	//fill the array ndigit with zeros
	for (j = 0; j < SIZE; j++)					
		ndigit[j] = 0;
    // delete spaces from the array str	
	for (i = strlen(str) - 1; i >= 0; i--)        // go from the end
	{
		if (str[i] == ' ' && status == OUT)
			str[i] = '\0';
		else if (str[i] != ' ' && status == OUT)
			status = IN;
		else if (str[i] == ' ' && status == IN){
			for (i; i < strlen(str); i++)
				str[i] = str[i + 1];
			status = OUT;
		}
	}
	if (str[0] == ' '){
		for (i = 0; i < strlen(str); i++)
			str[i] = str[i + 1];
	}
	printf("%s\n", str);     // for check
	// count and write two-digit number in the array ndigit	
	i = 1;
	while (i<= strlen(str))
	{
		if (str[i-1] >= '0' && str[i-1] <= '9' && status == OUT){
			first = str[i-1]- '0';
			status = IN;
		}		
		else if (str[i-1] >= '0' && str[i-1] <= '9' && status == IN){
			if (i % BIT == 0)
				ndigit[k++] = (first * 10 + (str[i - 1]-'0'));
			else
				first = (str[i - 1] - '0');
		}
		else if (str[i - 1] == '\n');
		else{
			puts("Input error. This sign is not a figure.\nRestart programm please.\n");
			return 1;
		}								
		i++;
	}
	if ((i-1) % 2 == 1 && status == IN)       // if the figure is one
		ndigit[k++] = first;
	// count sum in digit
	for (k = 0; k < SIZE; k++){
		if (ndigit[k] == 0);
		else
			sum += ndigit[k];
	}
	printf("Sum your number = %d\n", sum);
	return 0;
}

