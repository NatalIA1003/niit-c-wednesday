#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h> 
#include <stdlib.h>   
#define N 10			// size array
#define MAX 100			// range

int main()
{
	int arr[N];
	int i=0, j, start, end;							// for loop
	int sum = 0, num;
	int positive=0, negative=0;						// count positive and negative number
	srand(time(0));	
	while (i < N)
	{
		num = rand()% MAX + 1 - (MAX / 2);			// numbers from -50 to + 50
		if ((num < 0) && (negative < N / 2)) {
			arr[i] = num;
			negative++;
			i++;
		}
		if ((num >= 0) && (positive < N / 2)) {
			arr[i] = num;
			positive++;
			i++;
		}
	}	
	puts("Your the array: ");
	for (i = 0; i < N; i++)
		printf("%d ", arr[i]);
	putchar('\n');

	for (i = 0, j = (N-1); i < j; i++, j--)			// i go forvard, j go back
	{												
		if (arr[i] < 0 && arr[j] >= 0){
			start = i;
			end = j;
			break;
		}
		else if (arr[i] >= 0 && arr[j] < 0);
		else if (arr[i] < 0 && arr[j] < 0)
			i--;
		else if (arr[i] >= 0 && arr[j] >= 0)
			j++;
	}
	// count sum
	for (i = start; i <= end; i++)
		sum += arr[i];
	printf("First a negative figure from start = %d.\nFirst a positive figure from end = %d\n", arr[start], arr[end]);
	printf("Your a sum between %d and %d = %d\n", arr[start], arr[end], sum);
	return 0;
}


