#include<stdio.h>
#include<string.h>
#define IN 1
#define OUT 0

int main()
{
	char str[80];
	int status = OUT;     // flag inWord or outWord
	int len = 0;          // count word`s length
	int i = 0;            // for loop while
	int start1, end1, j;       // for loop print
	int max = 0, start, end;
	puts("Enter you string: ");
	fgets(str, 80, stdin);

	while (str[i])
	{
		if (str[i] != ' ' && status == OUT)
		{
			status = IN;   // flag changed
			start = i;     // save value i
		}
		else if ((str[i] == ' ' || str[i] == '\n') && status == IN)
		{
			end = i;								// save value i
			len = end - start;
			if (len > max){
				max = len;
				start1 = start;
				end1 = end;
			}
			status = OUT;
		}
		i++;
	}
	// print longest word
	for (j = start1; j < end1; j++)
		printf("%c", str[j]);
	printf(" - is the longest word len=%d\n", (end1 - start1));
	
	return 0;
}