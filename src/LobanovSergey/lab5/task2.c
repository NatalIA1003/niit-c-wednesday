#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>         //for cleaning the screen

#define ROW 20    
#define COL 40

void fill_array(char(*arr)[COL], int N);
void fill_first_quadrant(char(*arr)[COL], int N);
void fill_second_quadrant(char(*arr)[COL], int N);
void fill_other_quadrant(char(*arr)[COL], int N);
void print_array(char(*arr)[COL], int N);

int main()
{
    char picture[ROW][COL];
    int i, j;
    srand(time(0));
    clock_t now;
    while (1)
    {
        //fill the array with spaces
        fill_array(picture, ROW);

        //fill in the first quadrant
        fill_first_quadrant(picture, ROW);

        //fill in the second quadrant
        fill_second_quadrant(picture, ROW);

        //fill in the third and fourth quadrants
        fill_other_quadrant(picture, ROW);

        system("cls");

        //print array
        print_array(picture, ROW);

        now = clock();
        while (clock() < now + 2000);
    }
    return 0;
}

//fill the array with spaces or other symbols
void fill_array(char(*arr)[COL], int N)
{
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < COL; j++)
            arr[i][j] = ' ';
    return;
}

//fill in the first quadrant
void fill_first_quadrant(char(*arr)[COL], int N)
{
    int i, j, index;
    for (i = 0; i < N / 2; i++)
        for (j = 0; j < COL / 10; j++){
            index = rand() % COL / 2;
            arr[i][index] = '*';
        }
    return;
}

//fill in the second quadrant
void fill_second_quadrant(char(*arr)[COL], int N)
{
    int i, j;
    int index = COL - 1;
    for (i = 0; i < N / 2; i++)
    {
        for (j = 0; j < COL / 2; j++)
        {
            arr[i][index--] = arr[i][j];
        }
        index = COL - 1;
    }
    return;
}

//fill in the third and fourth quadrants
void fill_other_quadrant(char(*arr)[COL], int N)
{
    int i, j;
    int index = N - 1;
    for (i = 0; i < N / 2; i++)
    {
        for (j = 0; j < COL; j++)
        {
            arr[index][j] = arr[i][j];
        }
        index--;
    }
    return;
}

//print array
void print_array(char(*arr)[COL], int N)
{
    int i, j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < COL; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
    return;
}

