#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h> 
#define N 100
#define M 4096
#define OUT 0
#define IN 1

int getWords(char *string, char *pstring[]);
void random(char *pstring, int number);
void printWord(char *string);

int main()
{
    FILE *fp;
    char buf[N][M];
    char *pwords[N] = { 0 };
    int random_arr[N] = { 0 };                  // for random numbers 
    int i = 0, j, count;                        // count for words
    srand(time(0));

    fp = fopen("text.txt", "rt");
    check_file(fp);                             // check open or perror
    while (fgets(buf[i], M, fp))
    {
        count = getWords(buf[i++], pwords);     //return the number of words and fill pwords
        random(random_arr, count);              // creat random array in range number of words each string
        j = 0;
        while (j < count){
            printWord(pwords[random_arr[j++]]);
            putchar(' ');
        }
        putchar('\n');
    }
    fclose(fp);
    return 0;
}

// fill array of pstr first letters of words
int getWords(char *string, char *pstring[])
{
    int i = 0, count = 0;
    int status = OUT;

    while (*(string + i))
    {
        if (*(string + i) != ' ' && status == OUT)
        {
            pstring[count++] = string + i;
            status = IN;
        }
        else if (*(string + i) == ' ' || *(string + i + 1) == '\0')
            status = OUT;
        i++;
    }
    return count;
}

// creat random array
void random(int *string, int number)
{
    int i, random_num;

    for (i = 0; i < number; i++)
    {
        random_num = rand() % number;
        *(string + i) = random_num;
        for (int j = 0; j < i; j++)
        {
            if (*(string + j) == random_num)        // if number is already in array, so new random 
            {   
                i--;
                break;
            }
        }
    }
    return;
}

// print word from string with array of pstr till whitespace or end of string
void printWord(char *string)
{
    int i = 0;
    while (*(string + i))
    {
        if (*(string + i) == ' ' || *(string + i) == '\n' || *(string + i) == '\0' || *(string + i) == ',' || *(string + i) == '.')
        {
            return;
            putchar(' ');
        }
        putchar(*(string + i));
        i++;
    }
}