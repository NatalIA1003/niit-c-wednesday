#include <stdio.h>
#include <time.h> 
#define SIZE 1000
#define OUT 0
#define IN 1

int getWords(char *string, char *pstring[]);
void printWord(char *pstring[], int number);
void random(int *string, int number);

int main()
{
    char str[SIZE] = { 0 };
    char *pstr[SIZE] = { 0 };
    int random_arr[SIZE] = { 0 };   // for random numbers
    int count;                      // for count new words in pstr array
    int i = 0, random_num;

    srand(time(0));
    puts("Enter strig below, please: \n");
    fgets(str, SIZE, stdin);
    zero(str);                              // del '\n' from the end
    count = getWords(str, pstr);

    // creat random array 
    random(random_arr, count);

    // print random words  
    for (i = 0; i < count; i++)
        printWord(pstr, random_arr[i]);
    putchar('\n');
    return 0;
}

// fill array of pstr first letters of words
int getWords(char *string, char *pstring[])
{
    int i = 0,count = 0;
    int status = OUT;
    while (*(string + i))
    {
        if (*(string + i) != ' ' && status == OUT)
        {
            pstring[count++] = &*(string + i);
            status = IN;
        }
        else if (*(string + i) == ' ' || *(string + i + 1) == '\0')
            status = OUT;
        i++;
    }
    return count;
}

// print word from string with array of pstr till whitespace or end of string
void printWord(char *pstring[], int number)
{
    int i = 0;
    while (1)
    {
        putchar(*(pstring[number] + i));
        if (*(pstring[number] + i) == ' ' || *(pstring[number] + i) == '\0')
            return;
        i++;
    }
}

// creat random array 
void random(int *string, int number)
{
    int i, random_num;

    for (i = 0; i < number; i++)
    {
        random_num = rand() % number;
        *(string + i) = random_num;
        for (int j = 0; j < i; j++)
        {
            if (*(string + j) == random_num)        // if number is already in array, so new random 
            {
                i--;
                break;
            }
        }
    }
    return;
}

