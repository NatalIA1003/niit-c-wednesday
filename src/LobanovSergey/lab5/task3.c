#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h> 
#define N 100
#define M 4096
#define OUT 0
#define IN 1

int getWords(char *string, char *pstr2[]);
void printString(char *pstring);

int main()
{
    FILE *fp;
    char buf[N][M];
    char *pwords[N] = { 0 };                    // for words each string from file.txt
    int i = 0, count, num = 0;
    srand(time(0));

    fp = fopen("text.txt", "rt");
    check_file(fp);                             // check open or perror
    while (fgets(buf[i], M, fp))                // read next string from file
    {
        count = getWords(buf[i++], pwords);     //return the number of words and fill pwords 
        while (num < count)                     //take each word from pwords and work with him
            printString(pwords[num++]);         //print 
    }
    fclose(fp);
    return 0;
}

// fill array of pstr first letters of words
int getWords(char *string, char *pstring[])
{
    int i = 0, count = 0;
    int status = OUT;

    while (*(string + i))
    {
        if (*(string + i) != ' ' && status == OUT)
        {
            pstring[count++] = string + i;
            status = IN;
        }
        else if (*(string + i) == ' ' || *(string + i + 1) == '\0')
            status = OUT;
        i++;
    }
    return count;
}

// print word from string with array of pstr till whitespace or end of string
void printString(char *pstring)
{
    int i = 0;
    int length = 0, random_num;
    int word[N] = {0};                            //for random index of word

    // count length till whitespace or end of string
    while (*(pstring + i) != ' ' && *(pstring + i) != '.' && *(pstring + i) != ',' && *(pstring + i) != '\n' && *(pstring + i) != '\0')
        i++;
    length = i;

    // check length words
    if (length < 4)
        return;  

    // fill word[] with random numbers in range length without word[0] and word[length-1]
    for (i = 1; i < length - 1; i++)
    {
        random_num = rand() % length;
        if (random_num > 0 && random_num < length - 1)
        {
            word[i] = random_num;
            for (int j = 1; j < i; j++)
            {
                if (word[j] == random_num)        // if number is already in array, so new random
                {    
                    i--;
                    break;
                }
            }
        }
        else
            i--;
    }
    word[length - 1] = length - 1;                 // the last letter is not changed
    for (i = 0; i < length; i++)
        putchar(*(pstring + word[i]));
    putchar(' ');
    return;
}
