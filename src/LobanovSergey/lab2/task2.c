#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h> 
#include <stdlib.h>   

int main()
{
	int value, i=0;
	int guess;
	char ch;					// for buffer
	char win[] = "Congratulation, you are winner\n";
	
	srand(time(0));
	puts("Guess number from 1 to 100.\nYou have 7 attempts.\n");
	value = rand() % 100 + 1;   // range 1-100
	puts("Let`s go: ");
	printf("%d\n", value);
	while (1)
	{
		if (scanf("%d", &guess) == 1 && i < 6)   
		{					
			if (guess > value){
				printf("less ->");
				i++;
			}
			else if (guess < value){
				printf("more ->");
				i++;
			}
			else if (guess == value){
				printf("%s", win);
				return 0;
			}
		}
		else if (guess == value){
			printf("%s", win);
			return 0;
		}
		else if (i == 6 && guess != value)
		{
			puts("You are loser!\n");
			return 0;
		}
		else
			puts("Input Error! Enter again: ");
		do
			ch = getchar();
		while (ch != '\n' && ch != EOF);
	}	
	return 0;
}