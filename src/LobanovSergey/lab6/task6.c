#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define N 150
typedef unsigned long long ULL;

int fib_iter(ULL m1, ULL m2, int n)
{
    if (n == 1)
        return m1;
    else
        return fib_iter(m1 + m2, m1, n - 1);
}

ULL fib(int n)
{
    return fib_iter(1, 0, n);
}

int main()
{
    int i;
    for (i = 1; i < N; i++)
        printf("%d -> %25llu\n", i, fib(i));
    return 0;
}


