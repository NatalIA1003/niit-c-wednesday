#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>             //for malloc
#define MAX 1000
#define MIN 100
typedef unsigned char UC;
typedef unsigned long long ULL;

ULL recursionSum_arr(UC *arr, int size)
{
    if (size == 1)
        return *arr;
    else
        return (recursionSum_arr(arr, size / 2) + recursionSum_arr(arr + size / 2, size - size / 2));
}

ULL standartSum_arr(UC *arr, int size)
{
    int i = 0;
    ULL sum = 0;
    while (i < size)
        sum += *(arr + i++);
    return sum;
}

int power(int base, int n)
{
    if (n == 1)
        return base;
    else
        return base*power(base, n - 1);
}

// clean buffer
void buffer()
{
    char ch;
    do
        ch = getchar();
    while (ch != '\n' && ch != EOF);
}

int main(int argc, char *argv[])
{
    UC *arr;
    int N, M, i;
    double delta1, delta2;
    clock_t start, finish;
    srand(time(0));
    
    if (argc > 1)
        M = atoi(argv[1]);
    else
        while (1)
        {
            if (scanf("%d", &M) == 1)
                break;
            else{
                puts("Input Error!\n");
                buffer();
            }
        }

    N = power(2, M);
    arr = (UC*)malloc(sizeof(UC)*N);
    if (arr == 0)
        return 1;
    for (i = 0; i < N; i++)
        arr[i] = rand() % (MAX - MIN) + 1 + MIN;
    
    //recursion method
    start = clock();
    printf("The sum with recursion way is %llu\n", recursionSum_arr(arr, N));
    finish = clock();
    delta1 = (double)(finish - start)/CLOCKS_PER_SEC;
    
    //standart way
    start = clock();
    printf("The sum with standart way is %llu\n", standartSum_arr(arr, N));
    finish = clock();
    delta2 = (double)(finish - start)/CLOCKS_PER_SEC;
    (delta2 > delta1) ? printf("Time with standart way - %lf > %lf - Time with recursion way\n", delta2, delta1) :
                            printf("Time with standart way - %lf < %lf - Time with recursion way\n", delta2, delta1);
    free(arr);
    return 0;
}


