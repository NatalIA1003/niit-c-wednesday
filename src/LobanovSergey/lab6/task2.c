#include <stdio.h>
#define MIN 2
#define MAX 1000000
typedef unsigned long long   ULL;

ULL kollatca(ULL number, ULL sequence)
{
    sequence++;
    if (number == 1)
        return sequence;
    else if (number % 2 == 0)
        kollatca(number / 2, sequence);
    else
        kollatca(number * 3 + 1, sequence);
}

int main()
{
    ULL current_sequence, max_sequence = 0;
    ULL number;
    puts("Please wait...\n");
    for (int i = MIN; i <= MAX; i++)
    {
        current_sequence = kollatca(i, 0);
        if (current_sequence > max_sequence)
        {
            max_sequence = current_sequence;
            number = i;
        }
    }    
    printf("%llu(%llu) - is number who organized the longest sequence of Kollatca!\n", number, max_sequence);
    return 0;
}

