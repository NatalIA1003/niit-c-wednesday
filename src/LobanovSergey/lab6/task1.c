#include <stdio.h>
#define MAX 3
#define ROW 80
#define COL 80

//fill the array with spaces or other symbols
void fill_array(char(*arr)[COL], int N)
{
    int i, j;
    for (i = 0; i < N; i++)
        for (j = 0; j < COL; j++)
            arr[i][j] = ' ';
    return;
}

//fill the array with fractal symbols
void draw(int x, int y, int N, char(*arr)[COL])
{
    if (N == 0)
        arr[x][y] = '+';
    else
    {
        draw(x, y, N - 1, arr);
        draw(x + power(3, N - 1), y, N - 1, arr);
        draw(x - power(3, N - 1), y, N - 1, arr);
        draw(x, y + power(3, N - 1), N - 1, arr);
        draw(x, y - power(3, N - 1), N - 1, arr);
        
    }
}

//print array
void print_array(char(*arr)[COL], int N)
{
    int i, j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < COL; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
    return;
}

int power(int base, int n)
{
    if (n == 1)
        return base;
    else if (n == 0)
        return 1;
    else
        return base * power(base, n - 1);
}

int main(int argc, char *argv[])
{
    char fractal[ROW][COL];
    int N, x, y;                     //coordinates
    x = COL / 2;
    y = ROW / 2;
    if (argc > 1)
        N = argv[1];
    else
        N = MAX;
    fill_array(fractal, ROW);        //fill the array with spaces    
    draw(x, y, N, fractal);
    print_array(fractal, ROW);
    return 0;
}


