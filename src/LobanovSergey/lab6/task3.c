#include <stdio.h>
#include <time.h>
#define SIZE 20

//recursive function
int i = 0;                                  //???
void fromInt_toString(int N, char *arr)
{
    if (N < 0)
    {
        *(arr + i++) = '-';                 //save -
        N = -N;                             //remember N as positive
        fromInt_toString(N / 10, arr);
    }
    else if (N / 10 == 0){
        *(arr + i++) = N + '0';
        return;
    }
    else
        fromInt_toString(N / 10, arr);
    *(arr + i++) = N % 10 + '0';            //next number

}

//print array
void print_array(char *arr, int N)
{
    int i, j;
    for (i = 0; i < N; i++)
        putchar(arr[i]);
    putchar('\n');
    return;
}


int main(int argc, char *argv[])
{
    int number;
    char num[SIZE] = { 0 };
    srand(time(0));
    if (argc > 1)
        number = atoi(argv[1]);               
    else
        number = rand() % 10000;
    fromInt_toString(number, num);          //recursive function
    puts("Your string: \n");
    print_array(num, SIZE);                 //print
    return 0;
}



