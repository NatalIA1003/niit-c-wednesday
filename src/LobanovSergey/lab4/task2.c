#include <stdio.h>
#include<string.h>
#include<stdlib.h> 
#define N 100
#define IN 1
#define OUT 0

int main()
{
    char str[N];
    char *pstr[N] = { 0 };
    int count = 0;         // count *pstr
    int status = OUT;      // flag inWord or outWord
    int i=0, j=0;          // for loop while and for

    puts("Enter a line: ");
    fgets(str, 80, stdin);
    zero(str);

    while (str[i])
    {
        if (str[i] != ' ' && status == OUT)
        {
            status = IN;                            // flag changed
            pstr[count] = &str[i];                  // save value i in array pointers
            count++;                                
        }
        else if ((str[i+1] == ' ' || str[i+1] == '\0')  && status == IN)	
            status = OUT;
        i++;
    }
    // print from the end to the start
    for (i = count - 1; i >= 0; i--){
        j = 0;
        while (*(pstr[i] + j) != ' '  && *(pstr[i] + j) != '\0'){
            putchar(*(pstr[i] + j));
            j++;
        }
        putchar(' ');
    }
    putchar('\n');
    return 0;
}