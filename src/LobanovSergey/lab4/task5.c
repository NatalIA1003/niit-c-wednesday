#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include<string.h>
#include<stdlib.h>      // sort
#define M 4096       // for bufer
#define N 100 

int compare(void *str1, void *str2)
{
    return strlen(*(char**)str1) - strlen(*(char**)str2);
}

int main()
{
    FILE *fp, *outfp;
    char *pstr[N] = { 0 };
    char buf[N][M] = { 0 };
    int i = 0, k;

    fp = fopen("text.txt", "rt");
    outfp = fopen("textsort.txt", "wt");

    if (fp == 0 || outfp == 0){
        perror("File is not open!\n");
        return 1;
    }
    // read from text.txt and save every string in pstr
    while (fgets(buf[i], M, fp)){
        pstr[i] = buf[i];
        i++;
    }
    // sort buf
    qsort(pstr, i, sizeof(char*), compare);
    for (k = 0; k < i; k++)
        fprintf(outfp, pstr[k]);                 // sorttext.txt

    fclose(fp);
    fclose(outfp);
    return 0;
}

