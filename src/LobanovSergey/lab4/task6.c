#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define M 256 
#define MAX_RELATIVS 10
#define MIN_AGE 150

int main()
{
    char name[MAX_RELATIVS][M] = { 0 };
    char *young = 0;
    char *old = 0;
    int count = 0, i, relative;
    int age, max_age = 0, min_age = MIN_AGE;

    puts("How many relatives you have?\n");
    if (scanf("%d", &relative) == 1)
    {
        buffer();
        puts("Enter please, their names and ages like 'Boby 60':\n");
        while (count < relative  && *fgets(name[count], M, stdin) != '\n')
        {
            for (i = 0; i < strlen(name[count]); i++)
                if (name[count][i] >= '0' && name[count][i] <= '9')
                {
                    age = atoi(&name[count][i]);
                    if (age > max_age){
                        max_age = age;
                        old = name[count];
                        break;
                    }
                    else if (age < min_age){
                        min_age = age;
                        young = name[count];
                        break;
                    }
                    else
                        break;      // if age between min age and max age
                }
            count++;
        }
    }
    else{
        printf("Input Error\n");
        return 1;
    }
    printf("Oldest is - %sYoungest is - %s", old, young);

    return 0;
}