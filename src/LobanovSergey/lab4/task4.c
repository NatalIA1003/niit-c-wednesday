#include<stdio.h>
#include<string.h>
#define SIZE 100

int main()
{
    char str[SIZE];                // user str  
    char *pstr = str;
    int i = 0, j, max = 0;         // for loop                         
    int len = 1;                   //len for count the sentence

    puts("Enter a line: ");  
    fgets(str, SIZE, stdin);
    zero(str);                  // del ' ' and '\n' from the end str

    while (*(pstr + i))
    {
        if (*(pstr + i) == *(pstr + i + 1)){
            len += 1;
            if (len > max){
                max = len;
                j = i;
            }
        }
        else if (*(pstr + i) != *(pstr + i + 1))
            len = 1;
        i++;
    }
    for (i = 0; i < max; i++)
        putchar(*(pstr + j));
    printf("\t%d", max);
    putchar('\n');
    return 0;
}


