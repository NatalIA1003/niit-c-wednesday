/*Guess the number*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int val, num;
    srand(time(0));
    num=rand()%100+1;
    puts("Guess the number 1-100");
    scanf_s("%d", &val);
    while(val!=num)
    {
        if(val>num)
        {
            puts("The number is smaller! Try again:\n");
            scanf_s("%d", &val);
        }
        else if(val<num)
        {
            puts("The number is bigger! Try again:\n");
            scanf_s("%d", &val);
        }
    }
    puts("Correct!");

    return 0;
}